# DS_Python_Week5

Week 5 - Best Bus Stop

## [Click here for PYTHON CODE](Top_Best_Bus_Stops__-__Week_5.ipynb)


## Map 
#### Top 10 Bus stops using Haversine Distance
![](Bus_Map_Hav.JPG "Image Title")


#### Top 10 Bus stops using Manhattan Distance
![](Bus_Map_Man.JPG "Image Title")

#### Top 10 Altenative Bus stops using k-mean
![](Bus_Map_KMeans.JPG "Image Title")